# file:     bootstrap_config
# created:  13/09/16
# author:   Eugeniu Costetchi
import itertools
import json
import re

namespace_class = {
    "http://publications.europa.eu/resource/authority/": "nal",
    "http://publications.europa.eu/ontology/": "euvoc",
    "http://www.w3.org/1999/02/22-rdf-syntax-ns#": "lib",
    "http://www.w3.org/2002/07/owl#": "lib",
    "http://www.w3.org/2004/02/skos/core#": "lib",
    "http://lemon-model.net/lemon#": "lib",
    "http://purl.org/dc/terms/": "lib",
    "http://www.opengis.net/ont/geosparql#": "lib",
    "http://www.w3.org/ns/org#": "lib",
    "http://www.w3.org/2003/01/geo/wgs84_pos#": "lib",
    "http://purl.org/NET/c4dm/event.owl#": "lib",
    "http://creativecommons.org/ns#": "lib",
    "http://xmlns.com/foaf/0.1/": "lib",
    "http://lexvo.org/ontology#": "lib",
}

nal_map = {
    "http://publications.europa.eu/resource/authority/label-type/": "label-type",
    "http://publications.europa.eu/resource/authority/address-type/": "address-type",
    "http://publications.europa.eu/resource/authority/concept-status/": "concept-status",
    "http://publications.europa.eu/resource/authority/notation-type/": "notation-type",
    "http://publications.europa.eu/resource/authority/use-context/": "use-context",
    "http://publications.europa.eu/resource/authority/case-report/": "case-report",
    "http://publications.europa.eu/resource/authority/atu/": "atu",
    "http://publications.europa.eu/resource/authority/place/": "place",
    "http://publications.europa.eu/resource/authority/membership-classification/": "membership-classification",
    "http://publications.europa.eu/resource/authority/country/": "country",
    "http://publications.europa.eu/resource/authority/atu-type/": "atu-type",
    "http://publications.europa.eu/resource/authority/class-sum-leg/": "class-sum-leg",
    "http://publications.europa.eu/resource/authority/capital-classification/": "capital-classification",
    "http://publications.europa.eu/resource/authority/com_internal_event/": "com_internal_event",
    "http://publications.europa.eu/resource/authority/com_internal_consultation_type/": "com_internal_consultation_type",
    "http://publications.europa.eu/resource/authority/com_internal_procedure/": "com_internal_procedure",
    "http://publications.europa.eu/resource/authority/case-status/": "case-status",
    "http://publications.europa.eu/resource/authority/continent/": "continent",
    "http://publications.europa.eu/resource/authority/dataset-status/": "dataset-status",
    "http://publications.europa.eu/resource/authority/grammatical-gender/": "grammatical-gender",
    "http://publications.europa.eu/resource/authority/corporate-body/": "corporate-body",
    "http://publications.europa.eu/resource/authority/corporate-body-classification/": "corporate-body-classification",
    "http://publications.europa.eu/resource/authority/dataset-type/": "dataset-type",
    "http://publications.europa.eu/resource/authority/correction-status/": "correction-status",
    "http://publications.europa.eu/resource/authority/currency/": "currency",
    "http://publications.europa.eu/resource/authority/grammatical-number/": "grammatical-number",
    "http://publications.europa.eu/resource/authority/data-theme/": "data-theme",
    "http://publications.europa.eu/resource/authority/language/": "language",
    "http://publications.europa.eu/resource/authority/role/": "role",
    "http://publications.europa.eu/resource/authority/distribution-type/": "distribution-type",
    "http://publications.europa.eu/resource/authority/documenatation-type/": "documenatation-type",
    "http://publications.europa.eu/resource/authority/eu-budget-amount-status/": "eu-budget-amount-status",
    "http://publications.europa.eu/resource/authority/eu-budget-stage/": "eu-budget-stage",
    "http://publications.europa.eu/resource/authority/eu-budget-status/": "eu-budget-status",
    "http://publications.europa.eu/resource/authority/event/": "event",
    "http://publications.europa.eu/resource/authority/formjug/": "formjug",
    "http://publications.europa.eu/resource/authority/eu-programme/": "eu-programme",
    "http://publications.europa.eu/resource/authority/file-type/": "file-type",
    "http://publications.europa.eu/resource/authority/frequency/": "frequency",
    "http://publications.europa.eu/resource/authority/honorific/": "honorific",
    "http://publications.europa.eu/resource/authority/human-sex/": "human-sex",
    "http://publications.europa.eu/resource/authority/licence-domain/": "licence-domain",
    "http://publications.europa.eu/resource/authority/licence/": "licence",
    "http://publications.europa.eu/resource/authority/procedure/": "procedure",
    "http://publications.europa.eu/resource/authority/number-type/": "number-type",
    "http://publications.europa.eu/resource/authority/multilingual/": "multilingual",
    "http://publications.europa.eu/resource/authority/organization-type/": "organization-type",
    "http://publications.europa.eu/resource/authority/number/": "number",
    "http://publications.europa.eu/resource/authority/procresult/": "procresult",
    "http://publications.europa.eu/resource/authority/position-grade/": "position-grade",
    "http://publications.europa.eu/resource/authority/procjur/": "procjur",
    "http://publications.europa.eu/resource/authority/position-type/": "position-type",
    "http://publications.europa.eu/resource/authority/position-status/": "position-status",
    "http://publications.europa.eu/resource/authority/procjur-type/": "procjur-type",
    "http://publications.europa.eu/resource/authority/product-form/": "product-form",
    "http://publications.europa.eu/resource/authority/pub-theme/": "pub-theme",
    "http://publications.europa.eu/resource/authority/resource-type/": "resource-type",
    "http://publications.europa.eu/resource/authority/role-qualifier/": "role-qualifier",
    "http://publications.europa.eu/resource/authority/script/": "script",
    "http://publications.europa.eu/resource/authority/writing-system/": "writing-system",
    "http://publications.europa.eu/resource/authority/subject-matter/": "subject-matter",
    "http://publications.europa.eu/resource/authority/subdivision/": "subdivision",
    "http://publications.europa.eu/resource/authority/target-audience/": "target-audience",
    "http://publications.europa.eu/resource/authority/timeperiod/": "timeperiod",
    "http://publications.europa.eu/resource/authority/treaty-classification/": "treaty-classification",
    "http://publications.europa.eu/resource/authority/treaty/": "treaty",
}

lib_map = {
    # "http://publications.europa.eu/resource/authority/": "nal",
    "http://publications.europa.eu/ontology/": "euvoc",
    "http://www.w3.org/1999/02/22-rdf-syntax-ns#": "rdfs",
    "http://www.w3.org/2002/07/owl#": "owl",
    "http://www.w3.org/2004/02/skos/core#": "skos",
    "http://lemon-model.net/lemon#": "lemon",
    "http://purl.org/dc/terms/": "dct",
    "http://www.opengis.net/ont/geosparql#": "geosparql",
    "http://www.w3.org/ns/org#": "org",
    "http://www.w3.org/2003/01/geo/wgs84_pos#": "geowgs",
    "http://purl.org/NET/c4dm/event.owl#": "event",
    "http://creativecommons.org/ns#": "cc",
    "http://xmlns.com/foaf/0.1/": "foaf",
    "http://lexvo.org/ontology#": "lexvo",
}

# combine nals and external libraries into one dictionary
namespace_map = dict(itertools.chain(lib_map.iteritems(), nal_map.iteritems()))

# tabu nodes are the ones that shall be omited i.e. filtered out
tabu_nodes = ["http://publications.europa.eu/resource/authority/"]

config = {"namespace_map": namespace_map,
          "namespace_class": namespace_class,
          "tabu_nodes": tabu_nodes}


def bootstrap():
    with open("ns_config.json", "w") as outf:
        json.dump(config, outf, sort_keys=True)


if __name__ == "__main__":
    bootstrap()

    # r = r'[/#][^/#]*$'
    # print re.sub(r,"","http://adfdsfd/qweetertre#qwegf")
