# file:     triple2d3json
# created:  11/09/16
# author:   Eugeniu Costetchi

import csv
import json
import re


def load_configuration(filename="ns_config.json"):
    """
    :param filename: configuration file name, a json that shall contain three objects: namespace_map (a dict of
    namespace makings), namespace_class (a dict of namespace classifications), tabu_nodes (a list of namespaces to be
    omited from visualization)
    :return: three variables coresponding to each object in the configuration or empty
    """
    nm, mc, tn = {}, {}, []
    with open(filename, "r") as outf:
        conf = json.load(outf)
        if "namespace_map" in conf: nm = conf["namespace_map"]
        if "namespace_class" in conf: nc = conf["namespace_class"]
        if "tabu_nodes" in conf: tn = conf["tabu_nodes"]
        return nm, nc, tn
    return nm, nc, tn


# defining general variables
namespace_map, namespace_class, tabu_nodes = load_configuration()


def read_triples(filename="get_unique_namespaces.rq.csv", clean=True):
    """
    :param filename: file with triples
    :param clean:
    :return:
    """
    triples = []
    with open(filename, 'rb') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',', quotechar='"')
        for row in spamreader:
            triples.append(row)

    # throw away the header
    triples = triples[1:]
    triples = clean_triples(triples)
    return triples


# transforms the red variables into various json formats
def transform(f="get_unique_namespaces.rq.csv"):
    triples = read_triples(f)
    nodes, pairs = generate_nodes_and_pairs(triples)

    # ###########################
    # generating objects for simple D3/Cola display
    # ###########################
    # generate_D3_cola(nodes, pairs)

    # #############################
    # generating d3-process-map data
    # #############################
    gen_d3_process_map_data(nodes, pairs)


def gen_d3_process_map_data(nodes, pairs):
    # generating objects.json
    obj = map((lambda d: {"name": d[1], "type": d[2], "depends": depends_on(d, nodes, pairs)}), nodes)
    with open("d3-process-map-master/data/nal/objects.json", "w") as outf:
        json.dump(obj, outf)

    # generating the configuration file
    groups, constraints = generate_groups_and_constraints()

    config = {"title": "Namespace interdependence in NAL data",
              "graph": {
                  "linkDistance": 250,
                  "charge": -700,
                  "height": 1000,
                  "numColors": 12,
                  "labelPadding": {
                      "left": 3,
                      "right": 3,
                      "top": 2,
                      "bottom": 2
                  },
                  "labelMargin": {
                      "left": 3,
                      "right": 3,
                      "top": 2,
                      "bottom": 2
                  },
                  "ticksWithoutCollisions": 50
              },
              "types": groups,
              "constraints": constraints}
    with open("d3-process-map-master/data/nal/config.json", "w") as outf:
        json.dump(config, outf)


# def generate_D3_cola(nodes, pairs):
#     graph = {"nodes": [], "links": []}
#     graph["nodes"] = map((lambda d: {"name": d, "group": 0}), nodes)
#     # pprint(graph["nodes"])
#
#     graph["linksCola"] = map((lambda d: {"source": nodes.index(d[0]),
#                                          "target": nodes.index(d[1]), "value": 1}), list(pairs))
#
#     graph["linksD3"] = map((lambda d: {"source": d[0],
#                                        "target": d[1], "value": 1}), list(pairs))
#
#     with open("out/" + f + ".json", "w") as outf:
#         json.dump(graph, outf)


def generate_nodes_and_pairs(triples, tabu=tabu_nodes, ns_map=namespace_map, ns_class=namespace_class,
                             is_relation_a_namespace=False):
    """
    :param triples: return the set of unique namespaces
    :param tabu: omit namespaces listed in tabu
    :param ns_map: namespace mapping for short names
    :param ns_class: namespace mapping for classes
    :param is_relation_a_namespace: reduces the second column in triples (i.e. the link/predicate) to namespace as it
    contains the full property name and not it's namespace as the first and third columns do. it refers to the
    difference between http://www.w3.org/ns/org# and http://www.w3.org/ns/org#hasSite the latter being full name and
    the formed being the namespace only
    :return: a list of tuples (NOT dictionary of form {"long_name1":("short_name1","class1"), ...})
    """
    # generating unique nodes and pairs
    pairs = set()
    nodes = set()

    for t in triples:
        pairs.add(tuple([t[0], t[2]]))
        nodes.add(t[0])
        nodes.add(t[2])
        # if the property is not a namespace
        if is_relation_a_namespace:
            nodes.add(t[1])
            pairs.add(tuple([t[0], t[1]]))
        else:
            nodes.add(cut_last_ns_segment(t[1]))
            pairs.add(tuple([t[0], cut_last_ns_segment(t[1])]))

    # trivial node and pair filter
    nodes = filter(None, list(nodes))
    pairs = [p for p in list(pairs) if p[0] and p[1]]

    # reshaping nodes: extending node list with their class and shorted forms
    nodes = [(n, reduce_ns(n, namespace_map), type_of(n, namespace_class)) for n in nodes]
    # cleanup of node pairs
    nodes, pairs = clean_nodes_pairs(nodes, pairs)
    return nodes, pairs


def generate_groups_and_constraints(mapping=namespace_class, default_name="default"):
    """
    :param mapping:
    :param default_name:
    :return:
    """
    types = {}
    constraints = []

    for t in list(set(mapping.values())) + [default_name]:
        types[t] = {"long": t, "short": t}
        c = {
            "has": {"type": t},
            "type": "position",
            # "x": 0.5,
            # "y": 0.5,
            "weight": 0.7}
        constraints.append(c)
    return types, constraints


def depends_on(node, nodes, pairs):
    """
    :param nodes: te list of all nodes
    :param node: the target node
    :param pairs: the linked nodes
    :return: list of dependencies
    """
    deps = [p[1] for p in pairs if p[0] == node[0]]
    deps = [filter(lambda x: x[0] == d, nodes)[0][1] for d in deps]
    return deps


# def find_by_long_name(long_name, nodes):
#     """
#     :param long_name: long name URI of the node
#     :param nodes: list of tuples (long, short, class) for each node
#     :return: the tuple from nodes
#     """
#     return filter(lambda x: x[0] == long_name, nodes)


def type_of(node, mapping=namespace_class):
    """
    :param node:
    :param mapping:
    :return:
    """
    if ":" in node and not "://" in node:
        return node[:node.index(":")]

    for ns in mapping.keys():
        if ns in node:
            return mapping[ns]

    return "default"


def clean_nodes_pairs(nodes, pairs, filter_uris=tabu_nodes):
    '''
    :param nodes:
    :param pairs:
    :param filter_uris:
    :return:
    '''
    # clean nodes
    n = nodes
    p = pairs
    # check if the node is banned
    # n = [n for n in nodes if n[0] not in filter_uris]
    # clean pairs
    # remove self references
    # p = [s for s in pairs if s[0] != s[1]]  # and (s[0] is not None and s[1] is not None)
    # clear references to not defined nodes
    # p = [s for s in p if s not in filter_uris]
    return n, p


def clean_triples(triples):
    """
    :param triples:
    :return:
    """
    res = triples
    print len(res)
    # removing tabus i.e. banned URIs
    res = [s for s in res if not (s[0] in tabu_nodes or s[2] in tabu_nodes or s[1] in tabu_nodes)]
    print len(res)
    # removing self references
    res = [s for s in res if s[0] != s[2]]
    print len(res)
    # removing non classes, i.e default class
    res = [s for s in res if type_of(s[0]) != "default" and type_of(s[2]) != "default"]
    print len(res)
    return res


def cut_last_ns_segment(s): return re.sub(r'[/#][^/#]*$', "", s)


def reduce_ns(s, mapping):
    """
    :param s: full URI node name
    :param mapping: the namespace mapping
    :return: short
    """
    for i in mapping.keys():
        if i in s:
            return "" + mapping[i] + ":" + s.replace(i, "")
        if i[:-1] in s:
            return "" + mapping[i] + ":" + s.replace(i[:-1], "")
    return s


if __name__ == "__main__":
    transform()
